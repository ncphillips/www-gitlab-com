upgrade:
    reporter: fzimmer
    description: |
      - GitLab 12.0 merges the database changes made by Enterprise Edition over the years into Community Edition.
        As part of this work, we have also removed various old migrations. Users upgrading to their GitLab installation
        must first upgrade to the latest 11.11 patch release, then upgrade to 12.0.0. When upgrading to a future version such as 12.3.0,
        users must first upgrade to the latest 11.11 patch release as per our [recommended upgrade paths](https://docs.gitlab.com/ee/policy/maintenance.html#upgrade-recommendations). Failing to do so may result in migrations not being applied, which could lead to application errors.
        Omnibus installations already enforce upgrading to 12.0.0, and GitLab Helm Chart enforces a similar [upgrade path](https://docs.gitlab.com/charts/releases/2_0.html). Installations from source will have to take care of this manually.
      - GitLab 12.0 will use [Hashed Storage](https://docs.gitlab.com/ee/administration/repository_storage_types.html#hashed-storage) by default.
        This only affects new installations.
      - GitLab 12.0 will [automatically upgrade the PostgreSQL version to 10.7](https://docs.gitlab.com/omnibus/settings/database.html#upgrade-packaged-postgresql-server).
        - Users have the ability to skip the auto upgrade of PostgreSQL 10.7 creating `/etc/gitlab/disable-postgresql-upgrade`.
        - If you use [GitLab Geo](https://docs.gitlab.com/ee/administration/geo/replication/),
          the automatic PostgreSQL upgrade will be skipped on both the `primary` and all `secondary` nodes. We will [provide an upgrade path for Geo users in 12.1](https://gitlab.com/gitlab-org/gitlab-ee/issues/12164).
      - GitLab 12.0 will [enable JSON logging by default](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4102).
        We have also added documentation on how to [retain previous log format settings](https://docs.gitlab.com/omnibus/settings/logs.html#text-logging)
        where JSON is not desired.
      - Further information related to important Omnibus upgrade information in the [documentation](https://docs.gitlab.com/omnibus/update/gitlab_12_changes.html).