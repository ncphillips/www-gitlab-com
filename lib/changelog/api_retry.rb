# frozen_string_literal: true

module ApiRetry
  # API calls can fail to connect occasionally, which really shouldn't be a reason to fail entirely
  # Give them 5 goes, just to get past any transient network fail
  def api_retry
    tries = 0
    begin
      yield
    rescue Errno::ETIMEDOUT, Net::OpenTimeout => error # There may be more errors we should catch
      puts "Received a known retriable exception #{error.class}; have failed #{tries} times so far"
      retry if (tries += 1) < 5
      puts "Giving up after 5 retries"
      raise error
    # rubocop:disable Style/RescueStandardError
    rescue => error # Some other error: log the class but don't retry; we can investigate later
      puts "Received an #{error.class} exception; not retrying"
      raise error
    end
    # rubocop:enable Style/RescueStandardError
  end
end
